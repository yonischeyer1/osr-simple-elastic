const express = require('express')
const app = express()
const port = 3001
const axios = require('axios')
const cors = require('cors')
const bodyParser = require('body-parser')
app.use(cors({
	exposedHeaders: {
        "port": 3001,
        "bodyLimit": "100kb",
        "corsHeaders": ["Link"]
    }
}));
app.use(bodyParser.json({
	limit: "100kb"
}));
app.post('/search', (req, res) => {
    const data = req.body;
    const requestURL = `http://localhost:9200/auto/cars/_search`;
    console.log(req.body)
    axios.get(requestURL, {data}).then((result)=>{
        console.log(result)
        res.send(result.data)
    });
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))