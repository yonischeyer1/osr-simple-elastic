import { call, put, takeLatest } from 'redux-saga/effects'
import Api from './api'
import {  updateSearchResults, keywordSearchFaild } from './actions/search.actions';

function* fetchSubFolders(action) {
    try {
        const  keyword = action.payload
        let hits = yield call(Api.search, "auto", "cars", keyword)
        hits = hits.data["hits"]["hits"]
        yield put(updateSearchResults(hits))
    } catch (e) {
        yield put(keywordSearchFaild({message:{message:e.message,request:e.request}}));
    }
}

function* mySaga() {
    yield takeLatest("GET_SEARCH_RESULTS", fetchSubFolders);
}


export default mySaga;


