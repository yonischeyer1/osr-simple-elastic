import axios from 'axios';

const api = {
    insert_data: (data, index, doc_type) => {
        const requestURL = `http://localhost:9200/${index}/${doc_type}`;
        return axios.post(requestURL, {
            "brand": "nissan",
            "model": "micra",
            "hp": 130,
            "locations": ["israel", "europe", "south_america", "north_america"]
        });
    },
    search: (index, doc_type, keyword) => {
        const requestURL = `http://localhost:3001/search`;
        return axios.post(requestURL, {
            "query": {
                "multi_match": {
                    "query": keyword
                }
            }
        });
    }
};

export default api;