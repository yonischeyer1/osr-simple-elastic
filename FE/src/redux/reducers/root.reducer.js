import {combineReducers} from 'redux'
import { searchResults, errors} from './search.reducer'
import {reducer as formReducer} from 'redux-form'

export default combineReducers({
    errors,
    searchResults,
    form: formReducer
})