


export const searchResults = (state = [], action) => {
    switch (action.type) {
        case 'UPDATE_SEARCH_RESULTS':
            return [...action.payload ];
        default:
            return state
    }
};



export const errors = (state = {}, action) => {
    switch (action.type) {
        case 'KEYWORD_SEARCH_FAILD':
            state["KEYWORD_SEARCH_FAILD"] = action.payload.message
            return { ...state };
        default:
            return state
    }
};
