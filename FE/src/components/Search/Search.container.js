import React, { Component } from 'react';
import { connect } from 'react-redux'
import './Search.style.scss';
import { getSearchResults } from '../../redux/actions/search.actions'
import Api from '../../redux/api'
class Search extends Component {
    constructor(props) {
        super(props);
        this.keyDown = this.keyDown.bind(this);
    }
    componentDidMount() {
        Api.insert_data({
            "brand": "nissan",
            "model": "micra",
            "hp": 130,
            "locations": ["israel", "europe", "south_america", "north_america"]
        }, "auto", "cars")
    }
    keyDown(e){
        if (e.key === "Enter") {
            e.preventDefault();
            this.props.dispatch(getSearchResults(e.target.value))
        }
    }
    render() {
        return (
            <div className="search-container">
            <br/>
                <input type="text" className="search-input" onKeyPress={this.keyDown} onBlur={this.onBlur} /><br/>
                <ul>
                    {
                        this.props.searchResults.map((result,index) => {
                            return (<li key={index}>
                                {JSON.stringify(result)}
                            </li>)
                        })
                    }
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    searchResults: [...state.searchResults]
});

export default connect(mapStateToProps)(Search);
