import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import registerServiceWorker from './registerServiceWorker';
import Routes from './routes'

import {Provider} from "react-redux";
import {createStore, applyMiddleware} from 'redux';
import rootReducer from './redux/reducers/root.reducer';
import createSagaMiddleware from 'redux-saga';
import mySaga from './redux/sagas'
import { composeWithDevTools } from 'redux-devtools-extension';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware))
);
sagaMiddleware.run(mySaga);
ReactDOM.render(
        <Provider store={store}>
            <div>
                <Routes/>
            </div>
        </Provider>
    , document.getElementById('root'));
// registerServiceWorker();
